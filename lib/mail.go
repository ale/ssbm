package lib

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

var (
	sendmail = flag.String("sendmail", "/usr/sbin/sendmail", "Sendmail program")
)

// Mailer is the interface we use to send out emails. Having it as an
// interface allows for easier testing.
type Mailer interface {
	SendMail(sender string, recipients []string, data []byte) error
}

type sendmailMailer struct{}

// SendMail sends out an email to the specified recipients.
func (s *sendmailMailer) SendMail(sender string, recipients []string, data []byte) error {
	var buf bytes.Buffer
	w := bufio.NewWriter(&buf)
	fmt.Fprintf(w, "From: %s\n", sender)
	fmt.Fprintf(w, "To: %s\n", sender)
	fmt.Fprintf(w, "Bcc: %s\n", strings.Join(recipients, ", "))
	fmt.Fprintf(w, "Subject: [SSBM] update\n")
	fmt.Fprintf(w, "\n")
	w.Write(data)
	w.Flush()

	cmd := exec.Command(*sendmail, "-f", sender, "-t")
	cmd.Stdout = os.Stdout

	cmd.Stdin = &buf
	return cmd.Run()
}
