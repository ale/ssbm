// Edit data using an external editor.

package lib

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
)

func EditData(data string) (string, error) {
	tempf, err := ioutil.TempFile("", "ssbmedit")
	if err != nil {
		return data, err
	}
	filename := tempf.Name()
	defer os.Remove(filename)
	fmt.Fprintf(tempf, "%s", data)
	tempf.Close()

	editor := os.Getenv("EDITOR")
	if editor == "" {
		editor = "/usr/bin/vi"
	}
	// log.Printf("editing file %s\n", filename)
	cmd := exec.Command(editor, filename)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	err = cmd.Run()
	if err != nil {
		// log.Printf("got error: %s\n", err.Error())
		return data, err
	}

	dataStr, err := ioutil.ReadFile(filename)
	if err != nil {
		return data, err
	}
	return strings.TrimRight(string(dataStr), "\n"), nil
}
