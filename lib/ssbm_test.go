package lib

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

type testMailer struct {
	sent     map[string][]byte
	msgCount int
}

func (t *testMailer) SendMail(sender string, recipients []string, data []byte) error {
	t.msgCount++
	for _, recip := range recipients {
		t.sent[recip] = data
	}
	return nil
}

func newTestMailer() *testMailer {
	return &testMailer{
		sent:     make(map[string][]byte),
		msgCount: 0,
	}
}

// Generate a new temporary GPG key.
func generateNewTestGnupgKey(email string, bits int, root string) []byte {
	tmpHome := filepath.Join(root, "temp-gpg")
	os.Mkdir(tmpHome, 0700)
	os.Setenv("GNUPGHOME", filepath.Join(tmpHome, ".gnupg"))

	tmpf := filepath.Join(tmpHome, "gen-key.cmd")
	defer os.Remove(tmpf)

	file, err := os.Create(tmpf)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprintf(file, `
Key-Type: RSA
Key-Length: %d
Subkey-Type: RSA
Subkey-Length: %d
Name-Real: Test Key
Name-Email: %s

Expire-Date: 0

%%commit
`, bits, bits, email)
	file.Close()

	cmd := exec.Command(gpgPath, "--homedir", tmpHome, "--no-options", "--batch",
		"--gen-key", tmpf)
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		log.Fatal("Key generation failed")
	}

	key, err := exec.Command(gpgPath, "--homedir", tmpHome, "--export", "-a", email).Output()
	if err != nil {
		log.Fatal("Could not retrieve gpg key: %s", err)
	}

	return key
}

func createTestSsbmEnv(root, testUser string) (*Ssbm, []byte, *testMailer) {
	os.Mkdir(root, 0755)
	key := generateNewTestGnupgKey(testUser, 1024, root)
	tm := newTestMailer()
	s := NewSsbm(root)
	s.mailer = tm
	return s, key, tm
}

func TestSsbm_InitAndSave(t *testing.T) {
	defer os.RemoveAll(".test-ssbm")
	ssbm, key, mailer := createTestSsbmEnv(".test-ssbm", "test@example.com")
	err := ssbm.Init("test@example.com", "test@example.com", bytes.NewBuffer(key))
	if err != nil {
		t.Fatalf("Init() failed: %s", err)
	}

	if err := ssbm.Save(); err != nil {
		t.Fatalf("Save() failed: %s", err)
	}

	// Verify that no emails have been sent.
	if mailer.msgCount != 0 {
		t.Errorf("Some emails have been sent (%d)", mailer.msgCount)
	}
}

// TODO: Add more tests!
