// Key/value store.

package lib

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"os"
	"sort"
	"time"
)

// A simple UUID4 type.
type UUID [16]byte

func readUuid(dst []byte) {
	f, err := os.Open("/dev/urandom")
	if err != nil {
		log.Panic(err.Error())
	}
	defer f.Close()
	_, err = f.Read(dst)
	if err != nil {
		log.Panic(err.Error())
	}
}

// A value / version pair. Values are always strings.
type VersionedValue struct {
	// Version of the store at the time the value was set.
	Version uint32

	// Timestamp (for conflict resolution).
	Stamp int64

	// Unique ID (for merge deduplication).
	Id UUID

	// Value.
	Str string
}

func NewVersionedValue(version uint32, str string) *VersionedValue {
	v := VersionedValue{Version: version, Str: str, Stamp: time.Now().Unix()}
	readUuid(v.Id[0:16])
	return &v
}

// A slice of VersionedValue items that we can sort by version.
type VersionedValueArray []*VersionedValue

func (v VersionedValueArray) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

func (v VersionedValueArray) Len() int { return len(v) }

func (v VersionedValueArray) Less(i, j int) bool {
	// Compare versions first, then timestamps.
	if v[i].Version == v[j].Version {
		return (v[i].Stamp > v[j].Stamp)
	}
	return (v[i].Version > v[j].Version)
}

// A 'secret' is the data type stored in the key/value map.
// It contains a history of all the previous values so that we can
// resolve merge conflicts.
type Secret struct {
	// Current value (just an API optimization).
	Value string

	// Value history (should always be sorted by Version).
	Versions VersionedValueArray
}

// NewSecret returns a new Secret initialized with a value.
func NewSecret(value *VersionedValue) *Secret {
	s := Secret{Value: value.Str}
	s.Versions = make(VersionedValueArray, 1)
	s.Versions[0] = value
	return &s
}

// Set sets a new value for the secret.
func (s *Secret) Set(newValue *VersionedValue) {
	// We don't really need to sort the Versions array here, when
	// this method is called the Version in newValue will always
	// be greater or equal than the most recent element in Versions.
	newVersion := VersionedValueArray{newValue}
	s.Versions = append(newVersion, s.Versions...)
	s.Value = newValue.Str
}

// Remove duplicate entries from a VersionedValueArray.
func deduplicate(v VersionedValueArray) (out VersionedValueArray) {
	seen := make(map[UUID]bool)

	for _, value := range v {
		if _, ok := seen[value.Id]; !ok {
			seen[value.Id] = true
			out = append(out, value)
		}
	}
	return
}

// Merge merges the secret with another one, and performs conflict
// resolution by simply concatenating the history arrays,
// deduplicating entries, and sorting the result.
func (s *Secret) Merge(other *Secret) {
	s.Versions = deduplicate(append(s.Versions, other.Versions...))
	sort.Sort(s.Versions)
	s.Value = s.Versions[0].Str
}

// The key/value store.
type Store struct {
	// Current version.
	Version uint32

	// Secrets map.
	Secrets map[string]*Secret
}

func NewStore() *Store {
	store := Store{Version: 1}
	store.Secrets = make(map[string]*Secret)
	return &store
}

// Dump the store contents to standard output (for debugging).
func (store *Store) Dump() {
	fmt.Printf("Version: %d\n", store.Version)
	fmt.Printf("Secrets:\n")
	for key, secret := range store.Secrets {
		fmt.Printf("  %s = %s\n", key, secret.Value)
	}
}

// Set a secret.
func (store *Store) Set(key string, value string) {
	v := NewVersionedValue(store.Version, value)
	cur, ok := store.Secrets[key]
	if !ok {
		store.Secrets[key] = NewSecret(v)
	} else {
		cur.Set(v)
	}
}

// Fetch a secret.
func (store *Store) Get(key string) (*Secret, bool) {
	if s, ok := store.Secrets[key]; ok {
		return s, true
	}
	return nil, false
}

// Merge the Store with another one, automatically (and arbitrarily)
// resolving conflicts.
func (store *Store) Merge(other *Store) {
	// The resulting Version is the greatest of the two.
	if other.Version > store.Version {
		store.Version = other.Version
	}

	// Merge secrets.
	//
	// Note that this code won't attempt to remove existing
	// secrets.  There are a few approaches we could consider
	// about deletion: in the first one (currently implemented),
	// we never delete anything and you can emulate deletion by
	// setting a key to an empty value; this keeps the history
	// around. Alternatively, we could simply remove all secrets
	// whose version is < other.Version, which would also get rid
	// of the secret's history.
	for key, otherSecret := range other.Secrets {
		secret, ok := store.Secrets[key]
		if !ok {
			store.Secrets[key] = otherSecret
		} else {
			secret.Merge(otherSecret)
		}
	}
}

// Encode the Store to a string.
func (store *Store) Encode() (out []byte, err error) {
	// Increase the version whenever we dump the store.
	store.Version += 1

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	if err = enc.Encode(store); err != nil {
		return
	}
	out = buf.Bytes()
	return
}

// Decode a Store from a string.
func DecodeStore(data []byte) (*Store, error) {
	var store Store
	dec := gob.NewDecoder(bytes.NewBuffer(data))
	err := dec.Decode(&store)
	if err != nil {
		return nil, err
	}
	return &store, nil
}
