package lib

import (
	"os"
	"testing"
)

func TestConfig_SetAndGet(t *testing.T) {
	tempDir := ".config_test"
	defer os.RemoveAll(tempDir)
	c := NewFileConfig(tempDir)
	if err := c.Set("testkey", "testvalue"); err != nil {
		t.Fatal(err)
	}

	value, ok := c.Get("testkey")
	if !ok {
		t.Fatal("Key not found")
	}
	if value != "testvalue" {
		t.Fatal("Retrieved value not as expected: %s", value)
	}
}
