package lib

import (
	"bytes"
	"flag"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
)

var (
	// Flags.
	keyserver = flag.String("keyserver", "hkp://pool.sks-keyservers.net", "Keyserver URL")

	// Location of the GnuPG executable. Can be overridden by the user
	// with the GNUPG environment variable.
	gpgPath = getenvWithDefault("GNUPG", "gpg")
)

func getenvWithDefault(varName, defaultValue string) string {
	s := os.Getenv(varName)
	if s == "" {
		s = defaultValue
	}
	return s
}

// A GPG API with our own public ring.
//
// We need to keep friends' public keys in a separate keyring, but
// sign messages using the user's default one. The separate keyring is
// necessary because we don't have an easy way to check which key has
// signed an incoming message, so we rely on the GPG exit status.
type Gpg struct {
	home          string
	publicKeyring string
	selfKeyId     string
}

func NewGpg(home string, selfKeyId string) *Gpg {
	return &Gpg{
		home:          home,
		selfKeyId:     selfKeyId,
		publicKeyring: filepath.Join(home, "pubring.gpg"),
	}

}

func (g *Gpg) cmd(args ...string) *exec.Cmd {
	cmdLine := []string{
		"--no-greeting",
		"--primary-keyring", g.publicKeyring,
		"--yes",
	}
	cmdLine = append(cmdLine, args...)
	cmd := exec.Command(gpgPath, cmdLine...)
	log.Printf("Running: %s %v", gpgPath, cmdLine)
	cmd.Stderr = os.Stderr
	return cmd
}

func (g *Gpg) Init(selfKeyId string, r io.Reader) error {
	g.selfKeyId = selfKeyId

	log.Printf("initializing GPG directory %s", g.home)
	if err := os.Mkdir(g.home, 0700); err != nil {
		return err
	}

	// Import the user's public key in our keyring.
	cmd := g.cmd("--homedir", g.home, "--import")
	cmd.Stdin = r
	return cmd.Run()
}

// ReadEncryptedFile returns the contents of a file that has been
// encrypted with the user's private key.
func (g *Gpg) ReadEncryptedFile(path string) ([]byte, error) {
	data, err := g.cmd("--decrypt",
		"--no-default-keyring", "--keyring", g.publicKeyring,
		path).Output()
	if err != nil {
		return nil, err
	}
	return data, err
}

// WriteEncryptedFile encrypts some data with the user's own key, and
// saves it to a file.
func (g *Gpg) WriteEncryptedFile(path string, data []byte) error {
	if g.selfKeyId == "" {
		log.Fatal("GPG not initialized! Run 'ssbm init' and retry.")
	}
	cmd := g.cmd("--encrypt", "--always-trust",
		"-u", g.selfKeyId, "-r", g.selfKeyId,
		"--output", path, "-")
	cmd.Stdin = bytes.NewBuffer(data)
	return cmd.Run()
}

// EncryptAndSign encrypts and signs a message for many recipients.
func (g *Gpg) EncryptAndSign(data []byte, recipients []string) ([]byte, error) {
	args := []string{"--encrypt", "--sign", "--armor", "--always-trust", "-u", g.selfKeyId}
	for _, r := range recipients {
		args = append(args, "--recipient")
		args = append(args, r)
	}
	args = append(args, "-")
	cmd := g.cmd(args...)
	cmd.Stdin = bytes.NewBuffer(data)
	return cmd.Output()
}

func (g *Gpg) RecvKey(keyId string) error {
	return g.cmd("--keyserver", *keyserver, "--recv-keys", keyId).Run()
}

func (g *Gpg) DeleteKey(keyId string) error {
	return g.cmd("--delete-keys", keyId).Run()
}
