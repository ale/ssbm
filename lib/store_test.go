package lib

import (
	"testing"
	"time"
)

func TestVersionedValue(t *testing.T) {
	const version = 42
	const str = "test value"

	// Sanity checks.
	v := NewVersionedValue(version, str)
	if v.Version != version {
		t.Errorf("VersionedValue version %v, should be %v", v.Version, version)
	}
	if v.Str != str {
		t.Errorf("VersionedValue str %v, should be %v", v.Str, str)
	}

	// Test UUID generation.
	v2 := NewVersionedValue(version, str)
	if v.Id == v2.Id {
		t.Errorf("VersionedValue does not generate unique IDs")
	}
}

func TestSecret(t *testing.T) {
	// Sanity checks.
	v := NewVersionedValue(1, "value1")
	s := NewSecret(v)
	if s.Value != v.Str {
		t.Errorf("Secret.Value is %v, should be %v", s.Value, v.Str)
	}
	if len(s.Versions) != 1 {
		t.Errorf("len(Secret.Versions) is not 1 (%v)", len(s.Versions))
	}

	// Set it to a different value, check history.
	v2 := NewVersionedValue(2, "value2")
	s.Set(v2)
	if s.Value != v2.Str {
		t.Errorf("Secret.Set failed: value is %v, should be %v", s.Value, v2.Str)
	}
	if len(s.Versions) != 2 {
		t.Errorf("len(Secret.Versions) is not 2 (%v)", len(s.Versions))
	}
}

func TestSecret_Merge(t *testing.T) {
	v := NewVersionedValue(1, "value")
	s := NewSecret(v)
	s2 := NewSecret(v)
	v2 := NewVersionedValue(2, "newer value")
	s2.Set(v2)

	// Merging should deduplicate the 'v' value, and it should
	// recognize 'v2' as the latest value.
	s.Merge(s2)

	if len(s.Versions) != 2 {
		t.Errorf("Deduplication failure: len(Secret.Versions) is not 2 (%v)", len(s.Versions))
	}
	if s.Versions[0].Version != v2.Version {
		t.Errorf("Secret.Versions is not correctly sorted post-merge")
	}
	if s.Value != v2.Str {
		t.Errorf("Merge failed setting Secret.Value (%v, should be %v)", s.Value, v2.Str)
	}
}

func TestSecret_MergeAndResolveConflict(t *testing.T) {
	v := NewVersionedValue(1, "value")
	s := NewSecret(v)
	s2 := NewSecret(v)
	// Let the UNIX time change.
	time.Sleep(1 * time.Second)
	// New value, same version.
	v2 := NewVersionedValue(1, "newer value")
	s2.Set(v2)

	// Merging should deduplicate the 'v' value, and it should
	// recognize 'v2' as the latest value.
	s.Merge(s2)

	if len(s.Versions) != 2 {
		t.Errorf("Deduplication failure: len(Secret.Versions) is not 2 (%v)", len(s.Versions))
	}
	if s.Versions[0].Version != v2.Version {
		t.Errorf("Secret.Versions is not correctly sorted post-merge")
	}
	if s.Value != v2.Str {
		t.Errorf("Merge failed setting Secret.Value (%v, should be %v)", s.Value, v2.Str)
	}
}

func TestStore(t *testing.T) {
	store := NewStore()
	if store.Version != 1 {
		t.Errorf("Initialization of Store does not set Version to 1 (%v)", store.Version)
	}

	// Read a non-existing secret.
	s, ok := store.Get("key")
	if ok {
		t.Errorf("Store.Get() on non-existing key returns ok=true")
	}

	// Set and get a secret.
	store.Set("key", "value")
	s, ok = store.Get("key")
	if !ok {
		t.Fatalf("Store.Get() on existing key returns ok=false")
	}
	if s.Value != "value" {
		t.Errorf("Store.Get() returned bad secret data (%v)", s.Value)
	}

	// Set the same secret to a new value.
	store.Set("key", "new value")
	s, ok = store.Get("key")
	if !ok {
		t.Fatalf("key disappeared")
	}
	if s.Value != "new value" {
		t.Errorf("Store.Get() returns bad data after re-assignment (%v)", s.Value)
	}
}

func TestStore_Merge(t *testing.T) {
	// Test that keys are merged correctly.
	store := NewStore()
	store.Set("key1", "value1")
	store.Set("key2", "value2")
	store2 := NewStore()
	store2.Version = 2
	store2.Set("key1", "value3")
	store2.Set("new_key", "value")
	store.Merge(store2)

	if store.Version != 2 {
		t.Errorf("Merged Store version is not correct (%v, expecting 2)", store.Version)
	}

	// 'key1' should have the value from store2.
	s, ok := store.Get("key1")
	if !ok {
		t.Fatalf("Missing key, post-merge: key1")
	}
	if s.Value != "value3" {
		t.Errorf("Unexpected post-merge value for key1: %v", s.Value)
	}

	// 'key2' should still be in store.
	if _, ok = store.Get("key2"); !ok {
		t.Errorf("Missing key, post-merge: key2")
	}

	// 'new_key' should now be in store.
	s, ok = store.Get("new_key")
	if !ok {
		t.Fatalf("Missing key, post-merge: new_key")
	}
	if s.Value != "value" {
		t.Errorf("New key had bad value: %v (expecting 'value')", s.Value)
	}
}
