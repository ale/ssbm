package lib

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

type FileConfig struct {
	rootDir string
}

func NewFileConfig(rootDir string) *FileConfig {
	return &FileConfig{rootDir: rootDir}
}

func (fc *FileConfig) Set(key string, value string) error {

	// Ignore errors if the directory already exists.
	os.Mkdir(fc.rootDir, 0700)

	// Dump the value to file.
	data := fmt.Sprintf("%s\n", value)
	return ioutil.WriteFile(
		filepath.Join(fc.rootDir, key),
		[]byte(data),
		0600)
}

func (fc *FileConfig) Get(key string) (string, bool) {
	path := filepath.Join(fc.rootDir, key)
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return "", false
	}
	return strings.TrimSpace(string(data)), true
}
