// Secret Share By Mail

package lib

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type Ssbm struct {
	config     *FileConfig
	rootDir    string
	updatesDir string
	dbPath     string
	gpg        *Gpg
	mailer     Mailer
	store      *Store
}

func NewSsbm(rootDir string) *Ssbm {
	config := NewFileConfig(filepath.Join(rootDir, "config"))
	selfKeyId, _ := config.Get("self_key_id")

	s := Ssbm{
		config:     config,
		rootDir:    rootDir,
		updatesDir: filepath.Join(rootDir, "updates"),
		dbPath:     filepath.Join(rootDir, "db"),
		gpg:        NewGpg(filepath.Join(rootDir, ".gnupg"), selfKeyId),
		mailer:     &sendmailMailer{},
		store:      nil,
	}
	return &s
}

// Init initializes the local db and GPG keyrings. The arguments must
// refer to the identity of the local user and its GPG key.
func (s *Ssbm) Init(email string, keyId string, selfKey io.Reader) error {
	os.Mkdir(s.rootDir, 0700)
	os.Mkdir(s.updatesDir, 0700)
	if err := s.config.Set("self_email", email); err != nil {
		log.Printf("error setting config: %s", err)
	}
	if err := s.config.Set("self_key_id", keyId); err != nil {
		log.Printf("error setting config: %s", err)
	}
	return s.gpg.Init(keyId, selfKey)
}

// Read an encrypted datastore file.
func (s *Ssbm) readEncryptedStore(path string) (*Store, error) {
	data, err := s.gpg.ReadEncryptedFile(path)
	if err != nil {
		return nil, err
	}
	return DecodeStore(data)
}

// Load or initialize the datastore (processing all pending updates).
func (s *Ssbm) loadStore() error {
	if _, err := os.Stat(s.dbPath); err != nil {
		log.Printf("Creating new encrypted datastore in %s", s.dbPath)
		s.store = NewStore()
	} else {
		// Read the database, and process pending updates.
		store, err := s.readEncryptedStore(s.dbPath)
		if err != nil {
			return err
		}
		s.store = store
		err = s.processUpdates()
		if err != nil {
			return err
		}
	}
	return nil
}

// Save a datastore to disk.
func (s *Ssbm) saveStore(store *Store) ([]byte, error) {
	data, err := store.Encode()
	if err != nil {
		return nil, err
	}
	if err := s.gpg.WriteEncryptedFile(s.dbPath, data); err != nil {
		return nil, err
	}
	return data, nil
}

// GetStore returns the current datastore.
// It will lazily load the data and keep it in memory.
func (s *Ssbm) GetStore() *Store {
	if s.store == nil {
		err := s.loadStore()
		if err != nil {
			log.Fatal("Error loading encrypted store: %s", err)
		}
	}
	return s.store
}

// Save updates the datastore on disk, and sends email updates to the
// friend network.
func (s *Ssbm) Save() error {
	store := s.GetStore()
	data, err := s.saveStore(store)
	if err != nil {
		return err
	}

	// Get the list of emails and key IDs for the update.
	selfEmail, _ := s.config.Get("self_email")
	recipientEmails := []string{}
	recipientKeys := []string{}
	for email, keyId := range getFriends(store) {
		recipientKeys = append(recipientKeys, keyId)
		if email != selfEmail {
			log.Printf("sending update email to %s", email)
			recipientEmails = append(recipientEmails, email)
		}
	}

	// Encrypt and sign the outgoing update emails (but only if the
	// recipient list is not empty).
	if len(recipientKeys) > 0 {
		msg, err := s.gpg.EncryptAndSign(data, recipientKeys)
		if err != nil {
			return err
		}

		if err := s.mailer.SendMail(selfEmail, recipientEmails, msg); err != nil {
			return err
		}
	}

	return nil
}

// ReceiveUpdate stores an update for later merge processing.
func (s *Ssbm) ReceiveUpdate(r io.Reader) error {
	// Save data to a temporary file, and rename it to the final
	// destination atomically.
	file, err := ioutil.TempFile(s.updatesDir, ".tmp.up.")
	if err != nil {
		return err
	}
	defer file.Close()
	if _, err = io.Copy(file, r); err != nil {
		os.Remove(file.Name())
		return err
	}
	outFile := filepath.Join(s.updatesDir,
		fmt.Sprintf("up.%d", time.Now().UnixNano()))
	return os.Rename(file.Name(), outFile)
}

// Merge the current datastore with another one.
func (s *Ssbm) merge(newStore *Store) error {
	// Ensure that loadStore() has been called.
	if s.store == nil {
		panic("Ssbm.merge() called out-of-order")
	}

	oldFriends := getFriends(s.store)
	s.store.Merge(newStore)
	s.mergeFriends(oldFriends, getFriends(s.store))
	return nil
}

// Bootstrap sets up the database with *unencrypted* data received
// from a trusted peer. It will only succeed if the database is
// currently empty.
func (s *Ssbm) Bootstrap(in io.Reader) error {
	// Verify that the database is currently empty.
	if _, err := os.Stat(s.dbPath); err == nil {
		return errors.New("Datastore is not empty!")
	}

	// Decode new store from unencrypted data.
	data, err := ioutil.ReadAll(in)
	if err != nil {
		return err
	}
	store, err := DecodeStore(data)
	if err != nil {
		return err
	}

	// Merge into a new store. This is necessary so that the
	// friends keys are correctly imported in the GPG keyring,
	// which is done by merge().
	s.store = NewStore()
	if err = s.merge(store); err != nil {
		return err
	}

	_, err = s.saveStore(store)
	return err
}

// Merge all pending updates.
func (s *Ssbm) processUpdates() error {
	files, err := ioutil.ReadDir(s.updatesDir)
	if err != nil {
		return err
	}

	changed := false
	merged := []string{}
	for _, fileinfo := range files {
		if fileinfo.Mode().IsDir() {
			continue
		}
		if !strings.HasPrefix(fileinfo.Name(), "up.") {
			continue
		}
		log.Printf("processing update file %s", fileinfo.Name())
		fullPath := filepath.Join(s.updatesDir, fileinfo.Name())

		store, err := s.readEncryptedStore(fullPath)
		if err != nil {
			log.Printf("%s: Could not read file: %s", fileinfo.Name(), err)
			continue
		}
		if err := s.merge(store); err != nil {
			log.Printf("%s: Error merging update data: %s", fileinfo.Name(), err)
			continue
		}

		changed = true
		merged = append(merged, fullPath)
	}

	if changed {
		// Save the updated datastore but *do not* send out updates.
		log.Println("datastore updated, saving...")
		if _, err = s.saveStore(s.store); err != nil {
			return err
		}

		// Remove all the files that have been successfully processed.
		for _, fullPath := range merged {
			os.Remove(fullPath)
		}
	}

	return nil
}

// Friend management.
//
// 'Friends' are the people who you're sharing the secret data with.
// We need to maintain a GPG key for every one of them in our keyring,
// but at the same time we'd like to keep this data in the main Store,
// to persist it, share it, and take advantage of versioning.  So we
// store our map of email -> key_id pairs in a special attribute, with
// a very simple text encoding.
//
// High-level methods for manipulating the friend map are provided, so
// that we can synchronize the GPG keyring accordingly.

var kFriendAttrName = "__friends"

type FriendMap map[string]string

func NewFriendMapFromString(enc string) FriendMap {
	f := make(FriendMap)
	buf := bytes.NewBuffer([]byte(enc))
	reader := bufio.NewReader(buf)
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			break
		}
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 2 {
			continue
		}
		f[string(parts[0])] = string(parts[1])
	}
	return f
}

func (f FriendMap) ToString() string {
	buf := new(bytes.Buffer)
	for email, keyId := range f {
		fmt.Fprintf(buf, "%s %s\n", email, keyId)
	}
	return buf.String()
}

func getFriends(store *Store) FriendMap {
	var friends FriendMap
	fEnc, ok := store.Get(kFriendAttrName)
	if ok {
		friends = NewFriendMapFromString(fEnc.Value)
	} else {
		friends = FriendMap{}
	}
	return friends
}

func (s *Ssbm) AddFriend(email string, keyId string) {
	store := s.GetStore()
	friends := getFriends(store)
	friends[email] = keyId

	err := s.gpg.RecvKey(keyId)
	if err != nil {
		log.Printf("Could not import key ID %s: %s", keyId, err)
	} else {
		store.Set(kFriendAttrName, friends.ToString())
	}
}

func (s *Ssbm) DelFriend(email string) {
	store := s.GetStore()
	friends := getFriends(store)
	keyId, ok := friends[email]
	if !ok {
		return
	}
	err := s.gpg.DeleteKey(keyId)
	if err != nil {
		log.Printf("Could not delete key ID %s from GPG keyring: %s", keyId, err)
	}

	delete(friends, email)
	store.Set(kFriendAttrName, friends.ToString())
}

func (s *Ssbm) GetFriends() FriendMap {
	return getFriends(s.GetStore())
}

func (s *Ssbm) mergeFriends(oldFriends FriendMap, newFriends FriendMap) {
	for email, keyId := range oldFriends {
		if _, ok := newFriends[email]; !ok {
			if err := s.gpg.DeleteKey(keyId); err != nil {
				log.Printf("Could not delete key ID %s: %s", keyId, err)
			}
		}
	}
	for email, keyId := range newFriends {
		if _, ok := oldFriends[email]; !ok {
			if err := s.gpg.RecvKey(keyId); err != nil {
				log.Printf("Could not receive key ID %s: %s", keyId, err)
			}
		}
	}
}
