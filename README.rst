
++++++++++++++++++++++
Secret Sharing by Mail
++++++++++++++++++++++

SSBM is a simple system for lazy cospirators who want to share secrets
of some sort between a small group of mutually-trusted people, using
an existing GPG network of trust, and email as the communication
protocol.

The *secrets* don't really have a structure, they're just simple
key/value pairs.

The system works by doing versioned checkins and automatic conflict
resolution. The assumption is that the size of the secret database is
always going to be small enough to easily fit in an email message.


Installation and Setup
======================

Once you have a proper Go environment set up, this should be enough::

    $ go install git.autistici.org/ale/ssbm

The software needs to be installed along with your main GPG secret
key, as it will need to sign and decrypt data as yourself. For this
reason, it is recommended that you use an agent (unless you want GPG
to prompt you for the passphrase every single time you perform an ssbm
operation).

You need to find a way to feed certain messages from your INBOX
to the `ssbm` tool (for the `receive` command).

The program will look for the GnuPG executable as `gpg` in the current
path, but you can override this by setting the `GNUPG` environment
variable.


Step 1: Initialize SSBM
-----------------------

Pick the email address that you will use to receive the updates, and
the GPG key ID of your primary public key. Then, run the following
command::

    $ gpg -a --export ${KEYID} | ssbm init ${EMAIL} ${KEYID}

This will create the `~/.ssbm` directory.


Step 2: Setup email forwarding
------------------------------

You need to find a way to forward certain messages from the address
you selected above to the `ssbm` tool. There are many ways to do so,
depending on your specific mail setup. If, for instance, you're using
Procmail, you can put the following rule in your `.procmailrc`::

    :0H
    * ^Subject:.*\[SSBM\]
    | /usr/bin/ssbm receive

Messages intended for ssbm always have the `[SSBM]` string in the
*Subject* header. This cannot be modified for the moment, thus
limiting usage to a *single* shared datastore! This is a bug that will
be fixed soon.


Step 3 (optional): Import bootstrap data
----------------------------------------

If you have just been added to the shared datastore (i.e. someone
has run `add-friend` with your email and GPG key), you will have
received an encrypted email with `[SSBM]` in the subject. Verify
that the source is trusted and the signature is correct, decrypt
the data and pipe it into `ssbm bootstrap`.


Usage
=====

Manage users
------------

Add a user to the list of people who share the secrets::

    $ ssbm add-friend ${EMAIL} ${KEYID}

Delete a user from the list::

    $ ssbm del-friend ${EMAIL}

Further updates sent from this user will be ignored, and other users
will stop sending updates to him.


Get/Set values
--------------

Set a secret::

    $ ssbm set ${KEY} ${VALUE}

Retrieve its value::

    $ ssbm get ${KEY}

