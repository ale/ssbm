package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"sort"

	ssbm "git.autistici.org/ale/ssbm/lib"
)

var (
	// Errors.
	BadArguments = errors.New("Wrong number of arguments!")

	// Command-line args.
	showHistory = flag.Bool("history", false, "Show value history")
	homedir     = flag.String("homedir",
		filepath.Join(os.Getenv("HOME"), ".ssbm"),
		"SSMB directory")
)

func doInit(s *ssbm.Ssbm) error {
	if flag.NArg() < 3 {
		return BadArguments
	}

	email := flag.Arg(1)
	keyId := flag.Arg(2)

	err := s.Init(email, keyId, os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not initialize: %s", err)
		os.Exit(1)
	}
	fmt.Printf("done.\n")
	return nil
}

func doBootstrap(s *ssbm.Ssbm) error {
	if flag.NArg() != 1 {
		return BadArguments
	}

	err := s.Bootstrap(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error in bootstrap: %s", err)
		os.Exit(1)
	}
	fmt.Printf("done.\n")
	return nil
}

func doSet(s *ssbm.Ssbm) error {
	if flag.NArg() < 2 {
		return BadArguments
	}

	store := s.GetStore()
	key := flag.Arg(1)

	var value string
	if flag.NArg() > 2 {
		value = flag.Arg(2)
	} else {
		var curValue string
		if secret, ok := store.Get(key); ok {
			curValue = secret.Value
		}
		var err error
		if value, err = ssbm.EditData(curValue); err != nil {
			fmt.Fprintf(os.Stderr, "%s\n", err)
			os.Exit(3)
		}
	}

	store.Set(key, value)
	fmt.Printf("set new value for key %s\n", key)
	s.Save()
	return nil
}

func doGet(s *ssbm.Ssbm) error {
	if flag.NArg() != 2 {
		return BadArguments
	}

	key := flag.Arg(1)

	secret, ok := s.GetStore().Get(key)
	if !ok {
		fmt.Fprintf(os.Stderr, "Not found\n")
	} else {
		fmt.Printf("%s\n", secret.Value)
		if *showHistory {
			for _, item := range secret.Versions {
				fmt.Printf("  %s [%d] %s\n", item.Id, item.Version, item.Str)
			}
		}
	}
	return nil
}

func doDump(s *ssbm.Ssbm) error {
	s.GetStore().Dump()
	return nil
}

func doAddFriend(s *ssbm.Ssbm) error {
	if flag.NArg() != 3 {
		return BadArguments
	}

	email := flag.Arg(1)
	keyId := flag.Arg(2)
	s.AddFriend(email, keyId)
	s.Save()
	return nil
}

func doDelFriend(s *ssbm.Ssbm) error {
	if flag.NArg() != 2 {
		return BadArguments
	}

	email := flag.Arg(1)
	s.DelFriend(email)
	s.Save()
	return nil
}

func doListFriends(s *ssbm.Ssbm) error {
	if flag.NArg() != 1 {
		return BadArguments
	}

	for email, keyId := range s.GetFriends() {
		fmt.Printf("%s %s\n", keyId, email)
	}
	return nil
}

func doReceive(s *ssbm.Ssbm) error {
	if flag.NArg() != 1 {
		return BadArguments
	}

	err := s.ReceiveUpdate(os.Stdin)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error saving update data: %s\n", err)
		os.Exit(1)
	}
	return nil
}

type cmdFunc struct {
	fn    func(*ssbm.Ssbm) error
	usage string
	help  string
}

// Commands table.
var cmdMap = map[string]cmdFunc{
	"init": {doInit,
		"<MY_EMAIL> <MY_KEY_ID>",
		"Initialize a new SSBM environment. Must pass your public key on standard input."},
	"bootstrap": {doBootstrap,
		"",
		"Bootstrap the datastore, using data received from a trusted source. The data should be passed unencrypted to standard input."},
	"set": {doSet,
		"<KEY> [<VALUE>]",
		"Set a key/value pair."},
	"get": {doGet,
		"<KEY>",
		"Retrieve the value for a key."},
	"dump": {doDump,
		"",
		"Dump the entire contents of the key/value store to standard output."},
	"add-friend": {doAddFriend,
		"<EMAIL> <GPG_KEY_ID>",
		"Add a friend to the friend list."},
	"del-friend": {doDelFriend,
		"<EMAIL>",
		"Remove a friend from the friend list."},
	"list-friends": {doListFriends,
		"",
		"List all friends."},
	"receive": {doReceive,
		"",
		"Called from your email client, should receive an email message sent from SSBM on standard input."},
}

type cmdList []string

func (l cmdList) Len() int           { return len(l) }
func (l cmdList) Swap(i, j int)      { l[i], l[j] = l[j], l[i] }
func (l cmdList) Less(i, j int) bool { return l[i] < l[j] }

func printUsage() {
	fmt.Fprintf(os.Stderr, "Usage: ssbm <COMMAND> [<ARGS>]...\n\n")
	fmt.Fprintf(os.Stderr, "Options:\n")
	flag.PrintDefaults()
	fmt.Fprintf(os.Stderr, "\nKnown commands:\n\n")

	// Print the list of command in alphabetical order.
	cmds := make(cmdList, 0, len(cmdMap))
	for name, _ := range cmdMap {
		cmds = append(cmds, name)
	}
	sort.Sort(cmds)
	for _, cmdName := range cmds {
		cmdInfo := cmdMap[cmdName]
		fmt.Fprintf(os.Stderr, "%s %s\n  %s\n\n",
			cmdName, cmdInfo.usage, cmdInfo.help)
	}
}

func main() {
	flag.Usage = printUsage
	flag.Parse()

	if flag.NArg() < 1 {
		fmt.Fprintf(os.Stderr, "Not enough arguments!\n\n")
		printUsage()
		os.Exit(1)
	}

	cmd := flag.Arg(0)
	if cmd == "help" {
		printUsage()
		os.Exit(0)
	}

	// Load the secret store.
	s := ssbm.NewSsbm(*homedir)

	// Execute the requested command.
	cmdInfo, ok := cmdMap[cmd]
	if ok {
		err := cmdInfo.fn(s)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s\n\n", err)
			if err == BadArguments {
				printUsage()
			}
			os.Exit(1)

		}
	} else {
		fmt.Fprintf(os.Stderr, "Unknown command '%s'\n\n", cmd)
		printUsage()
		os.Exit(1)
	}

}
